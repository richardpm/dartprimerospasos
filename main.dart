main(List<String> args) {
  print("holaMundo");

  testingPrimitives();
  testingList();
  testingSet();
  testingFinalConst();
  testingArrowFunction();

}

testingPrimitives() {
  bool boolean = false;
  int integer = 9;
  String string = 'que tal';
  double decimalNumber = 6.9;

  print(boolean);
  print(integer);
  print(string);
  print(decimalNumber);
}

testingList() {
  List list = ['palta', 'limon', 'manzana'];
  print(list);
  list.add('naranja');
  print(list);
  list.removeAt(2); // remove manzana 0,1,2 <--
  print(list);
  list.remove('naranja');
  print(list);
  print(list.indexOf('limon'));
}

testingSet() {
  Set set = {'a', 'b', 'c', 'd'};
  print(set);
  set.remove('c');
  print(set);
  set.add('c');
  print(set);
  set.add('a'); // no errors
  print(set);
}

testingFinalConst() {
  final nombre = "Richard";
  print(nombre);

  final int numeroEntero = 69;
  print(numeroEntero);

  final bool esVacio = false;

  print(esVacio);

  const CONSTANTE = 3.1416;
  print(CONSTANTE);

  const String apellido = "Soto";
  print(apellido);
}

testingArrowFunction() {
  List lista = ['a', 'b', 'c'];


  lista.forEach((element) { 
// no entiendo el valor del foreach.. no retorna y recorre todo el arreglo, sin poder usar break
    if(element == 'b') {
       print(element);
    }
  });

  int sumarDosnum(a, b) => a + b;

  int resultado = sumarDosnum(3, 7);
  print(resultado); 
}